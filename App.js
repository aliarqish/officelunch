/**
 * Office Lunch
 * App
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

//import WelcomeScreen from './src/Components/ViewControllers/Welcome/WelcomeScreen';
//import OrderingScreen from './src/Components/ViewControllers/Ordering/OrderingScreen';
import NavigationRoutes from './src/Navigation/NavigationRoutes';

export default class App extends Component {
  render() {
    return (
      <View style={localStyles.container}>
          <NavigationRoutes />
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  container: {
    flex: 1
  },
});