/**
 * OfficeLunch
 * Ordering Screen
 */

 import React, { Component } from 'react';
 import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert
  } from 'react-native';
  import firebase from 'react-native-firebase';

  export default class OrderingScreen extends Component {

    //static navigationOptions = { header: null };

      render() {
          return (
            <View>
                <View>
                    <Text>Ordering Screen</Text>
                </View>

                <View>
                    <TouchableHighlight onPress={() => firebase.auth().signOut()
                                                        .then(() => this.props.navigation.navigate('WelcomeScreen'))}>
                        <Text>Logout</Text>
                    </TouchableHighlight>
                </View>
            </View>
          );
      }
  }