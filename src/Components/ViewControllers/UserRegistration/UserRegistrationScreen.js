/**
 * OfficeLunch
 * UserRegistration Screen
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import firebase from 'react-native-firebase';

export default class WelcomeScreen extends Component {

    state = {
        email: '',
        password: '',
        confirmPassword: '',
      };

    static navigationOptions = { header: null };

    onButtonPress = () => {

        firebase.auth().createUserWithEmailAndPassword(this.state.email,this.state.password)
            .then(() => this.props.navigation.navigate('OrderingScreen'))
            
    }

    render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
              <Text style={styles.headingText}>OFFICE LUNCH</Text>
              <Text style={styles.subHeadingText}>Food ordering made easy</Text>
        </View>
        <View style={styles.body}>
        <View style={styles.userRegisterStyle}>
            <Text style={styles.userRegisterText}>Register</Text>
        </View>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(email) => this.setState({email})}
              value={this.state.email}
              autoCapitalize='none'
            />
        </View>
        
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
            />
        </View>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Confirm password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(confirmPassword) => this.setState({confirmPassword})}
              value={this.state.confirmPassword}
            />
        </View>

        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={this.onButtonPress.bind(this)}>
          <Text style={styles.loginText}>Register</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.buttonContainer} onPress={() => this.props.navigation.navigate('WelcomeScreen')}>
            <Text>Already have an account? Login</Text>
        </TouchableHighlight>

        <View>
            <Text style={styles.errorTextStyle}>{this.state.error}</Text>
        </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  header: {
    flex: 0.25,
    alignItems: "center",
    paddingTop: 80,
  },
  body: {
    flex: 0.75,
  },
  headingText: {
    fontSize: 46,
    fontWeight: '500',
  },
  subHeadingText: {
    fontSize: 16,
    fontWeight: '300',
    fontStyle: 'italic'
  },
  userRegisterStyle: {
    alignItems: "center",
    marginBottom: 15,
  },
  userRegisterText: {
    fontSize: 22,
    fontWeight: '300',
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#00b5ec",
  },
  loginText: {
    color: 'white',
  },
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
});