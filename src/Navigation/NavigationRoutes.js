/**
 * OfficeLunch
 * NavigationRoute
 */

import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
//Welcome
import WelcomeScreen from '../Components/ViewControllers/Welcome/WelcomeScreen';

//Ordering
import OrderingScreen from '../Components/ViewControllers/Ordering/OrderingScreen';

//UserRegistration
import UserRegistrationScreen from '../Components/ViewControllers/UserRegistration/UserRegistrationScreen';


const NavigationRoute = createStackNavigator({
    WelcomeScreen: WelcomeScreen,
    OrderingScreen: OrderingScreen,
    UserRegScreen: UserRegistrationScreen,
},
{
    initialRouteName: 'WelcomeScreen'
});

export default createAppContainer(NavigationRoute);